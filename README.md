# WordPress Secure by Design Starter Kit
**Cybersecurity is everybody's responsibility, especially software developers'.**

Project template with a list of development tasks created following Security by Design practices, tactics, government guidelines and OWASP Cheat Sheets:
- [Issue board](https://gitlab.com/testudio/wp-security-by-design-starter-kit/-/boards) - issues grouped by labels, eg Project management, Development, CI, API security
- WordCamp Sydney 2024 [Secure by Design presentation slides](https://docs.google.com/presentation/d/1prKAF6UhaU2lQVtD7ayIc9UjoW_DdcC_-CwN7p_-WqM/edit?usp=sharing)
- [WordCamp Sydney 2024 Video](https://www.youtube.com/watch?v=CF5-YAYiEFY)

## Secure by Design project
Secure by Design project promotes:
- Security-first mindset
- Better project planning and estimation
- Preparation for penetration testing
- Compliance with gov't guidelines and regulations

## Awareness
Be aware of how wide spread this issue is:
- [Scams on internet](https://www.scamwatch.gov.au/)
- [Check if your personal data has been stolen](https://haveibeenpwned.com/)
- [Australian Cyber Security Centre's Alerts](https://www.cyber.gov.au/about-us/view-all-content/alerts-and-advisories)
- [Wordfence: Vulnerabilities Catalogue](https://www.wordfence.com/threat-intel/vulnerabilities)

## Government Guidelines
- [Secure by Design by CISA (USA)](https://www.cisa.gov/securebydesign)
- [Secure your website by ACSC (AU)](https://www.cyber.gov.au/resources-business-and-government/maintaining-devices-and-systems/system-hardening-and-administration/web-hardening/secure-your-website)
- [Guidelines for Software Development by ACSC (AU)](https://www.cyber.gov.au/resources-business-and-government/essential-cyber-security/ism/cyber-security-guidelines/guidelines-software-development)
- [Subscribe to alerts by ACSC (AU)](https://www.cyber.gov.au/about-us/view-all-content/alerts-and-advisories)

## Technical Guidelines:
- [Pen. test guide by OWASP](https://owasp.org/www-project-web-security-testing-guide/v42/)
- [Authentication CheatSheet by OWASP](https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html)

## WordPress documentation
- [WordPress security team](https://wordpress.org/about/security/)
- [Hardening WordPress](https://wordpress.org/documentation/article/hardening-wordpress/) 
- [Security releases](https://wordpress.org/news/category/security/)

## Learn 
How to learn to to write secure code?
- Coding standards and best practices
- Subscribe to security alerts
- Get involved with community and conferences
- Training and certification
- Practice by reviewing/reading code/fix of existing security issues

### Coding standards and best practices
- [PHP the right way](https://phptherightway.com/)
- [Clean code PHP](https://github.com/piotrplenik/clean-code-php)
- [Clean code JS](https://github.com/ryanmcdermott/clean-code-javascript)
- [WordPress coding standards](https://developer.wordpress.org/coding-standards/wordpress-coding-standards/php/)
- [WPScan CLI scanner](https://wpscan.com/wordpress-cli-scanner/)

### Subscribe to security alerts
- WordPress and plugins: https://www.wordfence.com/subscribe-to-the-wordfence-email-list/
- Everything (used by SAST tools and `composer audit`): https://github.com/advisories (subset of CVE)
Based on [CVE](https://www.cve.org/) (Common Vulnerabilities and Exposures):
- AU ACSC: https://www.cyber.gov.au/about-us/view-all-content/alerts-and-advisories 
- NZ CERTNZ: https://www.cert.govt.nz/about/about-us/?subscribe/

### Get involved with community and conferences
- [Join OWASP](https://owasp.org/membership/)
- Attend a security conference (or watch presentations): 
  - [RSA](https://www.rsaconference.com/)
  - [OWASP Global](https://www.youtube.com/@OWASPGLOBAL/videos)
  - [NDC {Security}](https://www.youtube.com/watch?v=BkigVNNSurI&list=PL03Lrmd9CiGey4D3-wb_2SWTmLJtGHC_j)
- Attend your local OWASP meetup

### Training and certification
- [Google Cybersecurity Certificate](https://grow.google/certificates/cybersecurity)
- [SecureFlag (free with OWASP membership)](https://www.secureflag.com/owasp)
- [PortSwigger Web security academy](https://portswigger.net/web-security)
- [Developing Secure Software (LFD121) course](https://training.linuxfoundation.org/training/developing-secure-software-lfd121/)

### Practice: Review WordPress' security issues and fixes
Schedule security review sessions with your teammates: 
- select a recent security issue 
- view the corresponding code change: identify an issue and discuss the solution
- review your own codebase for potential exposure

#### Arbitrary PHP code execution
CVE-2024-8353: GiveWP – Donation Plugin and Fundraising Platform <= 3.16.1 - Unauthenticated PHP Object Injection
- [Issue](https://www.wordfence.com/threat-intel/vulnerabilities/wordpress-plugins/give/givewp-donation-plugin-and-fundraising-platform-3161-unauthenticated-php-object-injection)
- [Solution](https://github.com/impress-org/givewp/compare/3.16.0...3.16.1)

#### Access bypass
CVE-2023-28121: Access bypass on payment plugin
- [Issue](https://www.cve.org/CVERecord?id=CVE-2023-28121)
- [Patch on github](https://github.com/Automattic/woocommerce-payments/compare/5.6.1...5.6.2)

#### XSS
CVE-2024-10108: WPAdverts – Classifieds Plugin <= 2.1.6 - Unauthenticated Stored Cross-Site Scripting via adverts_add Shortcode
- [Issue](https://www.wordfence.com/threat-intel/vulnerabilities/wordpress-plugins/wpadverts/wpadverts-classifieds-plugin-216-unauthenticated-stored-cross-site-scripting-via-adverts-add-shortcode) 
- [Solution](https://plugins.trac.wordpress.org/changeset?sfp_email=&sfph_mail=&reponame=&new=3178088%40wpadverts%2Ftrunk%2Fincludes%2Fajax.php&old=2957213%40wpadverts%2Ftrunk%2Fincludes%2Fajax.php&sfp_email=&sfph_mail=)

### Review a plugin
Follow [OWASP: Code review guide](https://owasp.org/www-project-code-review-guide/)

Code review - plugin structure, coding standards, anything that's not quite right:
- are there any debug messages left in the code?
- how module is configured? How secrets are managed/stored?
- are there any specific permissions defined in this plugin? Are permissions being checked?
- is there any usage of direct database calls? (DB calls are very expensive, slow and can be exploited in SQL Injection attacks)
- input validation - are there any usages of unescaped query string, DB queries, is data validated on server side before processing, etc?
- are dependencies up to date?
- how error handling and logging is performed?
- review previous security issues and how they were patched
- etc etc etc

>
> **Security issues are to be reported to WordPress security team**
> - follow instructions in security.txt - https://wordpress.org/security.txt
> - report security issues in core - https://make.wordpress.org/core/handbook/testing/reporting-security-vulnerabilities/ 
> - report security issues in plugins - plugins@wordpress.org
  

## References:
- [CISA (USA): Secure-by-design project](https://www.cisa.gov/securebydesign)
- [CISA (USA): Secure-by-design principles](https://www.cisa.gov/resources-tools/resources/secure-by-design)
- [CISA (USA): Product Security Bad Practices Catalogue](https://www.cisa.gov/resources-tools/resources/product-security-bad-practices)
- [ACSC (AUS): Secure your website](https://www.cyber.gov.au/resources-business-and-government/maintaining-devices-and-systems/system-hardening-and-administration/web-hardening/secure-your-website)
- [ACSC (AUS): Guidelines for Software Development](https://www.cyber.gov.au/resources-business-and-government/essential-cyber-security/ism/cyber-security-guidelines/guidelines-software-development)
- [NCSC (UK): Secure development and deployment guidance](https://www.ncsc.gov.uk/collection/developers-collection)
- [CVE: WordPress-related CVE Catalogue](https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=wordpress)
- [CVE: Wordpress: Vulnerability Statistics](https://www.cvedetails.com/vendor/2337/Wordpress.html)
- [Wordfence: Vulnerabilities Catalogue](https://www.wordfence.com/threat-intel/vulnerabilities)
- [OWASP: Web Security Testing Guide (WSTG)](https://owasp.org/www-project-web-security-testing-guide/v42/) - Penetration testing checklist
- [OWASP: Cheat Sheets](https://cheatsheetseries.owasp.org/)
- [OWASP: API Security](https://owasp.org/www-project-api-security/)
- [OWASP: Top 10](https://owasp.org/www-project-top-ten/)
- [OWASP: 10 Proactive Controls](https://owasp.org/www-project-proactive-controls/)
- [OWASP: Application Security Verification Standard (ASVS)](https://github.com/OWASP/ASVS) 
- [OWASP: Code review guide](https://owasp.org/www-project-code-review-guide/)
- [New U.K. Law Bans Default Passwords on Smart Devices Starting April 2024](https://thehackernews.com/2024/04/new-uk-law-bans-default-passwords-on.html)
- [Hardening WordPress](https://wordpress.org/documentation/article/hardening-wordpress/) 
- [Bedrock - a WordPress boilerplate in git](https://github.com/roots/bedrock)
- [WordPress GitHub Integration: A Beginners' Guide](https://blog.hubspot.com/website/wordpress-github-integratio)
- [Beginner’s Guide to Using Git with WordPress](https://www.wpbeginner.com/beginners-guide/beginners-guide-to-using-git-with-wordpress)

## Support
- Report bugs, request features, propose suggestions by creating Gitlab Issues.
- Use merge requests (MRs) to contribute to this project.

## License
CC0 1.0 Universal - CC0 public domain
